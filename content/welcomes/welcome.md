---
id: 0
title: Welcome to Pullman Bunker Bay Resort
image: ~
---

It is our pleasure to welcome you to award winning Pullman Bunker Bay Resort.

Whether you are travelling for business or leisure, we look forward to making your stay a memorable one.

Our team is here to help and make personal recommendations on their favorite activities and places within the region.

Warm regards

#### Leighton Yates

General Manager 0414 044 261
