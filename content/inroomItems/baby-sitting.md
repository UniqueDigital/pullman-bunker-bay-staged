---
title: Baby Sitting
---

The Welcome Desk can assist you to organise this service through local accredited and screened babysitters.
