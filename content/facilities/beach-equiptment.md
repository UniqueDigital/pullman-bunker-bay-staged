---
title: Beach Equipment
---

Beach chairs, beach umbrellas, buckets and spades and an assortment of beach games are available for resort guests from the Welcome Desk.

Usage is complimentary, however a bond will be placed on your room account until items are returned.

Dial 2
