---
id: 1
title: Ikou De Stress Organic Facial
details: 90mins | $230
---

Stress has a powerful effect on ageing and skin sensitivity, and this calming, restorative facial focuses on collagen regeneration, healing, nourishing and rebuilding skins elasticity and protective hydration using Organic & Wild-Harvested Australian fruit and flower extracts. This sensory facial incorporates aromatherapy scalp, foot, arm and lower leg massage with Geranium, Lavender & Clementine for a deeply relaxing experience.
