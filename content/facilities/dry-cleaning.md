---
title: Dry Cleaning
---

Laundry services are available Monday to Friday.

The price list is on the resort laundry docket. Laundry bags and docket are conveniently located in your wardrobe.

Please place your items in the laundry bag with your docket and dial 2 for collection.

Collection of garments should be arranged prior to 9 00am for garments to be returned the same day.
