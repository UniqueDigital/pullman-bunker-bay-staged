---
id: 3
title: iKou Deep Cell Hydration Facial
details: 60mins | $170
---

Quench thirsty skin and restore elasticity and radiance in this deeply hydrating facial. Featuring super-antioxidant, native Australian rainforest Crown of Gold. Renewal begins with a focus on effective exfoliation. Moisture balance is restored with a deep cell low molecular weight Hyaluronic Acid infusion compress and luxurious double masques for instant, visible results leaving skin radiant, nourished and revitalise.
