---
title: Tennis Courts
---

We have two tennis courts available at the entrance to the resort. Bookings are essential and can be made at the Welcome Desk. 

Tennis racquets and balls are complimentary, and the court may be booked for one hour sessions on the hour.

Dial 2
