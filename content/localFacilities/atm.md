---
title: ATM (Automated Teller Machine)
---

A number of banks operate Monday to Friday in Dunsborough.

The nearest ATM machines are located in Dunsborough within the Coles shopping complex.

For more information, please contact the Welcome Desk.

Dial 2
