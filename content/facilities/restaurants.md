---
title: Restaurants
---

Other Side of the Moon serves refined and inventive cuisine made of quality locally sourced produce. To make a dinner reservation, or to view the menu, please make your way to the profile:

<nuxt-link to="/otherSideOfTheMoon" class="button-primary">Click Here</nuxt-link>

Open for lunch and dinner from 11.00am till 9pm nightly.

For recommendations on restaurants and wineries in the region, please contact the Welcome Desk.

Dial 2
