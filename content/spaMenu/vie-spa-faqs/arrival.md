---
id: 0
title: Arrival
details:
---

Please arrive at least 15 minutes prior to your appointment to give yourself time to enjoy our relaxation lounge and complete your consultation form.
