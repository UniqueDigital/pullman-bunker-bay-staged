---
id: 2
title: Have you reserved a table?
image: reserve-a-table.jpg
linkTitle: ~
link: ~
externalLinkTitle: Book a Table
externalLink: https://booking.resdiary.com/TablePlus/Standard/OtherSideOfTheMoonAccor7119/2832?utm_source=Website&utm_medium=other-side-of-the-moon-restaurant-in-bunker-bay&utm_campaign=BookaTable&utm_term=OtherSideOfTheMoonAccor7119
---

Due to existing restrictions imposed by the WA Government, capacity is still reduced so we recommend you make a booking as soon as possible.
