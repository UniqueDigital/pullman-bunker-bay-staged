---
id: 2
title: Baby Balance
details: 60mins | $145
---

Treat yourself during your pregnancy with this specialised massage to relieve tension while supporting both your body and bump. Suitable after 12 weeks.
