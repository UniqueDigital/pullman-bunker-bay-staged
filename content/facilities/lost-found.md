---
title: Lost And Found
---

Our housekeepers will keep any object found in your room after departure. Please contact the Welcome Desk to advise us of your lost or misplaced items.

Dial 2
