---
id: 3
title: Cape Naturaliste Vineyard
image: hero.jpg
alt: hero Image
phone: '+61897552538'
website: https://www.capenaturalistevineyard.com.au
email: ~
addressTitle: 'Address:'
addressDesc: 1 Coley Rd (enter from Caves Rd)
addressDescTwo: Yallingup, WA, 6282
openingHoursTitle: Open 7 Days
openingHoursDetails: 10:30am - 5:00pm
offerTitle: Join our Wine Club
offerDescription: See our website for more details!
deliveryOptionTitle: ~
deliveryfee: ~
deliveryDisclaimer: ~
facebook: ~
instagram: ~
twitter: ~
bookurl: ~
gallery:
  - hero.jpg
  - image1.jpg
  - image2.jpg
  - image3.jpg
  - image4.jpg
---

#### AWARD WINNING WINES

##### London International Wine Show Winner - Trophy for Best Red from a Single Vineyard 2011

##### James Halliday - 5 Star Vineyard

To the south of Cape Naturaliste, in a picturesque valley behind Smiths Beach, lies Cape Naturaliste Vineyard. Overlooking the valley is a 19th Century stone inn. Once the staging point for the coaches on the Perth to Margaret River route, the now renovated inn provides a delightful backdrop to the vineyard. The property became a dairy farm in the late 1800's and the old stone milking shed is now the cellar door.

The valley's rich variety of soil types are ideal for growing premium wine grapes, and our maritime location, and a biodynamic philosophy and proactive farming techniques, allows us to preclude the use of herbicides and pesticides. We pride ourselves on providing the highest quality, naturally produced wines available.
