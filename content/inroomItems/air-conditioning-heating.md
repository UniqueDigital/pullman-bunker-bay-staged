---
title: Air conditioning & heating
---

Your room has an individual thermostat that can be adjusted to the temperature desired. We recommend setting the temperature to 23 degrees.

For cooling choose the snowflake option, for heating choose the sun option.

For any assistance dial 2
