---
title: Washing Machine
---

To use the washing machine located in your villa, please follow the operation sequence below:

- Load clothes into the machine
- Add washing powder and close the lid
- Turn the power switch on
- Using the smart-touch cycles push button to select the appropriate wash cycle

- Select a suitable washing temperature by pressing the Wash Temp push button
- Select the water level by pressing Water Level push button
- Select the spinning speed by pressing the Spin Speed push button
- Press Start/Pause to commence cycle

As the washing cycle advances, the wash progress is displayed.
