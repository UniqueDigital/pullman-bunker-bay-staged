---
title: Do Not Disturb
---

For your privacy, why not hang out the “do not disturb” sign on your villa entrance?

Please note: the housekeeping staff will not service your room if your room indicates a do not disturb. To arrange service to your room, please contact housekeeping prior to 5pm.

Dial 3
