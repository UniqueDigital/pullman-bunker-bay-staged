---
id: 3
title: access to red hot rooms where discounts can be up to 50% OFF
first: check.svg
second: check.svg
third: check.svg
---

Save up to 50% on your hotel or resort accommodation with our Red Hot Room offers. Exclusive to Accor Plus members, Red Hot Rooms offer great savings on a wide selection of Accor hotels.
