---
title: Emergencies
---

We are here to assist you 24 hours a day. Please call the Welcome Desk.

Dial 2

For external emergencies, contact emergency services (police/ ambulance/fire). Dial 000\*

<a href="tel:000" class="button-primary">Call 000</a>

\*If calling from your room, you will need to dial 0 for an outside line, then 000.
