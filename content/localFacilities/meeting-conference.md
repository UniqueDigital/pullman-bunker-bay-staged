---
title: Meeting Rooms / Conferences
---

For meetings, conferences and special events Pullman Bunker Bay Resort has function spaces to provide the perfect backdrop for your event.

Our sales team will be pleased to give you all the information you may require.

Dial 2

<a href="tel:+61797569108" class="button-primary">Call Direct</a>
