---
title: Car Park
---

Car parking facilities are located within the resort grounds. Please contact the Welcome Desk to assist you with transferring your luggage to your car on departure.

Dial 2
