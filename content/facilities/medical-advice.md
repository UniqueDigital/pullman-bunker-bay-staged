---
title: Medical Advice
---

For non-urgent medical needs during your stay, for example if you are feeling unwell, forgot a prescription or wish to get medical advice:

1. Contact the reception desk by dialling “2” to let the team know you would like to consult a medical professional.
2. Reception will give you the option of a free telephone consultation or access to a selection of AXA-certified healthcare providers for a face-to-face consultation.
