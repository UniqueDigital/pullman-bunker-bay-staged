---
title: Books and Board Games
---

Several books from a variety of genres are available for guests to read during their stay. Feel free to check out the range in the connection lounge. Board games are available for resort guests from the Welcome Desk. Usage is complimentary, however a bond will be placed on your room account until items are returned.

Dial 2
