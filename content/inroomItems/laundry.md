---
title: Laundry
anchor: laundry
---

Laundry services are available Monday to Friday.

The price list is on the resort laundry docket. Laundry bags And docket are conveniently Located in your wardrobe.

Please place your items in the laundry bag with your docket and dial 2 for collection.

Collection of garments should Be arranged prior to 9.00am For garments to be returned the same day.
