---
title: Dishwasher
---

You have a dishwasher in your villa and dishwasher tablets are under the sink.

Should you require assistance with operating your dishwasher, contact the Welcome Desk.

Dial 2
