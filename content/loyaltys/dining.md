---
id: 5
title: up to 50% off dining across 1400 restaurants in asia pacific
first: check.svg
second: check.svg
third: check.svg
---

Savour culinary delights from around the world at over 1,400 hotel restaurants across 20 countries. Your Accor Plus membership brings you up to 50% off the food bill and 15% off drinks in Asia.
