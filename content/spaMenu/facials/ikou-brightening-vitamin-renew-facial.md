---
id: 2
title: I Kou Brightening Vitamin Renew Facial
details: 60mins | $170
---

An active facial to boost luminosity, improve elasticity and deliver powerful Anti-Ageing results. Advanced, ultra-intensive blend of antioxidant Astaxanthin with vitamins and emollients to protect against environmental damage and deliver age-defying results. Collagen is boosted and cells are renewed with vitamin A rich Australian Banksia Seed Oil.
