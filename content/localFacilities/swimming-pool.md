---
title: Swimming Pool
---

Our resort boasts a 25 metre wet edge infinity lap pool heated to 26° Celsius all year round. Pool towels are located in the pool area. Please make yourself familiar with the pool rules and regulations located at the pool, which is open during sunlight hours.

During peak summer periods, the Pool Bar is open, offering snacks and refreshments.

Outside of these times, items can be ordered via the Pool Bar telephone and delivered by Villa catering to the poolside.
