---
id: 6
title: 15% off your drinks bill in asia
first: check.svg
second: check.svg
third: check.svg
---

Your Accor Plus membership brings you 15% off drinks in Asia.
