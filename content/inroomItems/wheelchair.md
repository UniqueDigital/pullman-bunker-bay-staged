---
title: Wheelchairs
---

Should you require a wheelchair during your stay, or wish to be located in a special access room, please contact the Welcome Desk.

Dial 2
