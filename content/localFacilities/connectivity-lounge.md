---
title: Connectivity Lounge
---

A working area is available in the lobby, 24 hours a day with internet connection, Excel, Word, webcams and printing facilities. Please contact the Welcome Desk.

Dial 2
