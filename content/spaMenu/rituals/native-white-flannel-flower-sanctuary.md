---
id: 2
title: Native White Flannel Flower Sanctuary
details: 120mins | $310
---

Unwind and allow your body to receive the healing benefits of
deep relaxation. Inspired by the beauty and healing properties of
the Australian white flannel flower, this popular ritual is a complete head-to- toe degustation of iKOU treatments. Connect with the inner joy that lives within you, and restore a radiant all-over glow. From head to toe, unwind, breathe, relax.

- Organic coconut & jasmine body polish
- Australian white flannel floral hydrating cocoon
- Organic radiance facial
- Full body white flannel flower soufflé
- Foot restoration massage
