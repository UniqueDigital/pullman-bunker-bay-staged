---
title: Internet Connection
---

All Pullman guests recieve complimentary internet access.

Accor Live Limitless loyalty members receive complimentary access to our high speed internet service.

A wireless connection is available throughout the resort. For further assistance contact the Welcome Desk.

Dial 2
