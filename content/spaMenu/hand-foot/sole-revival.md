---
id: 0
title: SOLE REVIVAL
details: 60mins | $110
---

Our signature pedicure. Indulge in a warm foot soak, smoothing
exfoliation, nail care, massage and mask. Includes a colour polish.
