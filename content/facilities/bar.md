---
title: BAR
---

The Bar at Pullman Bunker Bay Resort is located in the main building and is open every day from 11.30am to late.

Why not try a cocktail in its relaxing atmosphere? A bistro style menu is also available from 12.00pm to 10.00pm.
