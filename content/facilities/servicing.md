---
title: Servicing
---

Your villa will be serviced by housekeeping between the hours of 10:00am and 3:00pm. See do not disturb if you require room service outside of these hours.
