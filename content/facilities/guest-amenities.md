---
title: Guest Amenities
---

A wide range of guest amenities are available by contacting our housekeeping service. Dial 3

- Additional Linen and Towels
- Bathrobes
- Disposable Razor
- Flower Vase
- Guest Stationery
- Hangers
- Rollaway Beds
- Sewing Kit
- Shower Cap
- Slippers
- Toothbrush/Toothpaste
