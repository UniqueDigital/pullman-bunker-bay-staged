---
title: Fire
---

In case of an alarm, keep calm and follow the instructions given by the resort staff.

An Emergency Evacuation plan is located on the internal side of each guest room door. Please review the plan and familiarise yourself with the nearest fire exit and strategy.

To advise the resort of a fire contact the Welcome Desk.

Dial 2
