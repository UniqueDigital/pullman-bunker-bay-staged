---
title: Movies – In-house system
---

A selection of movies is available and can be operated by using your remote control. A charge applies to in-house movies. Please refer to on-screen instructions. Should you wish to block this service, please call the Welcome Desk.

Dial 2
