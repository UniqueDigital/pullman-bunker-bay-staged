---
title: Address
---

If you are a guest of ours for several days your relatives will be able to contact you at the following address 24 hours-a-day:

**Pullman Bunker Bay Resort**
42 Bunker Bay Rd\
Po Box 565\
Naturaliste WA 6281\
Western Australia

**_Phone_**: +61 8 9756 9100\
**_fax_**: +61 8 9756 9103\
**_email_**: h8775@accor.com

When away from your accommodation, we suggest that your contacts leave a message on your voicemail (incorporated in the phone) or at the Welcome Desk. A red light will blink on the phone when a message is left.
