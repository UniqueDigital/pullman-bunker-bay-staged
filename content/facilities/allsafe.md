---
title: Allsafe
---

Welcoming, safeguarding and taking care of others is at the very heart of what we do and who we are at Accor. Due to the Covid-19 pandemic, we have elevated our cleaning & hygiene processes even further by launching our ALLSAFE Label which represents some of hospitality’s most stringent cleaning standards. Feel welcome to speak to one of our ALLSAFE Officers whom oversee the process of the Label should you have any questions via our Assistant Managers at Reception Desk.
