---
id: 1
title: Forest Revival
details: 120mins | $310
---

Breathe new life into your body and soul and enjoy a fresh start with this uplifting ritual. A sea salt scrub reveals radiant skin while preparing the body and relaxing the mind to fully enjoy the benefits of full body massage that focuses on releasing tension and leaves you as energised as a walk in the forest.

- Quandong deep conditioning scalp cocoon
- Australian lemon myrtle body scrub
- Reviving full body massage
- Foot restoration massage with cucumber mask
