---
id: 1
title: Couples Bath and Massage
details: 90mins | $385
---

Enjoy a romantic shared experience in our tranquil couple’s suites with this massage treatment for two.

- Couple’s Aromatherapy Bath Soak - a blissful way to re-connect before your massage
- Vie Massage - Our signature full body massage combining rhythmic movements & organic iKOU oils
