---
title: Safe
---

There is a safe located in your villa wardrobe. Please note that the resort is not liable for loss of personal items left in your room. To protect your valuables please follow the instructions for your safe carefully. Should you require assistance, contact the Welcome Desk.

Dial 2
