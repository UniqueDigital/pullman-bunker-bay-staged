---
id: 0
title: Vie Body Scrub
details: 30mins | $85
---

A customised body exfoliation that is rich in essential fatty acids and antioxidants, infused with oils that promote cell renewal and polish away dull skin.
