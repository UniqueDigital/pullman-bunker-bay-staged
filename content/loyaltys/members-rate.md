---
id: 1
title: Exclusive Member's rate of 10% off the best available public room rate
first: check.svg
second: check.svg
third: check.svg
---

Enjoy more nights for less by accessing your exclusive Accor Plus member’s rate all year round. Check out the hotel list or the Accor App and book your next trip on your member’s rate.
