---
id: 0
title: Back, Neck & Shoulder
details: 30mins | $85
---

Target the areas that carry stress with an organic iKOU essential oil blend.
