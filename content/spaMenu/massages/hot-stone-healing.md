---
id: 1
title: Hot Stone Healing
details: 60mins | $160 - 90mins | $205
---

Expertly guided hot basalt stones combined with geranium and clementine oils and fluid movements. 90min option includes additional foot renewal exfoliation.
