---
id: 7
title: member exclusive experiences such as cooking with celebrity chefs
first: check.svg
second: check.svg
third: check.svg
---

Meet and greet your sporting idols, congratulate the cast members of the latest theatre sensation in person, learn how to slice, dice and sauté with a Masterchef. Do things you’ve never done before – thanks to your Accor Plus membership.
