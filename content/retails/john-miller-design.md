---
id: 0
title: John Miller Design
image: hero.jpg
alt: hero Image
phone: '+61897566336'
website: https://www.johnmillerdesign.com
email: 'info@johnmillerdesign.com'
addressTitle: Two Locations
addressDesc: 51 Marrinup Drive, Yallingup WA
addressDescTwo: 135 Bussel Hwy, Margaret River WA
openingHoursTitle: Closed Sundays
openingHoursDetails: 'Mon - Sat: 10am - 4pm'
offerTitle: ~
offerDescription: ~
deliveryOptionTitle: ~
deliveryfee: ~
deliveryDisclaimer: ~
facebook: ~
instagram: ~
twitter: ~
bookurl: ~
gallery:
  - hero.jpg
  - image1.jpg
  - image2.jpg
  - image3.jpg
  - image4.jpg
  - image5.jpg
---

John Miller Design is an iconic Australian artisan jewellery workshop renowned for creating bespoke wedding rings, unique engagement rings and wearable pieces of art. Here at John Miller Design we use only genuine sapphires and rubies and "conflict free" - lab certified diamonds. Our work is original, quality craftsmanship, unconditionally guaranteed. John Miller Design prides itself on excellent, personalised customer service, competent repairs, remakes and alterations.

We make beautiful things that are meaningful and real.
