---
id: 0
title: OCEANIC BODY & MIND BALANCE
details: 120mins | $310
---

De-stress the mind, balance the body and restore energy and hydration in this soothing head to toe signature rejuvenation ritual.
Focusing on mood lifting benefits using essential oils to promote a feeling of wellbeing, this calming treatment will leave you feeling relaxed and hydrated.

- Australian jojoba & orange full body scrub
- Relaxing aromatherapy full body massage
- Wellbeing radiance facial
- Soothing foot treatment