---
title: Pets
---

As we are adjacent to national park, we regret to advise no pets are allowed to stay in the resort. Exemptions can be sought for seeing eye dogs, please advise our staff to arrange amenities.

Dial 2
