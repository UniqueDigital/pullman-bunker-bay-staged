---
title: Leisure and Activities
---

Bunker Bay beach walk across the lake on the resort's boardwalk to the pristine waters of Bunker Bay is perfect for walking, running, swimming, fishing and surfing.

For information on other nearby surf beaches, wineries and breweries, cape to cape track walks, whale watching, wildflowers, wine tasting, yoga, kids club, shopping or galleries, contact our Welcome Desk. Maps and local tours can be arranged for you should you wish to go sightseeing.

Dial 2
