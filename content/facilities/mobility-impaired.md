---
title: Mobility Impaired
---

Some of our rooms are made to be accessible for the mobility impaired. A lift is also available in the main building to assist guests with access to the lower bar area. Refer to the Welcome Desk for further assistance.

Dial 2
