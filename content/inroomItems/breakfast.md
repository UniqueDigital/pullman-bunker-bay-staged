---
title: Breakfast
---

We are pleased to offer you breakfast 7 days a week from 7.00am to 10.30am at the Other Side of the Moon Restaurant and Bar.

Barista coffee is available at the bar.
