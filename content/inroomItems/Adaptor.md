---
title: Adaptor
---

If you are having difficulties charging your phone or if you require an adaptor, our Welcome Desk will be happy to lend you one.

Dial 2
