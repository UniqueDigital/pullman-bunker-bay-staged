---
title: Country Life Farm
image: hero.jpg
alt: hero Image
phone: '+61897553707'
website: https://www.countrylifefarm.com.au
email: 'farmerjohno@msn.com'
addressTitle: 'Address:'
addressDesc: '1694 Caves Road, Dunsborough WA 6281'
addressDescTwo: '(1km towards Yallingup from Dunsborough)'
openingHoursTitle: 'Open 7 Days'
openingHoursDetails: '9:30am - 5:00pm'
offerTitle: ~
offerDescription: ~
deliveryOptionTitle: ~
deliveryfee: ~
deliveryDisclaimer: ~
facebook: ~
instagram: ~
twitter: ~
bookurl: ~
gallery:
  - hero.jpg
  - image1.jpg
  - image2.jpg
  - image3.jpg
  - image4.jpg
  - image5.jpg
  - image6.jpg
  - image7.png
  - image8.png
---

Country Life Farm is a family run and owned business and has been operating since 1989!

Country Life Farm is a great day out for the whole family. You can hand feed all the friendly farm animals any time of the day.

Rabbits, guinea pigs, kangaroos, llamas, alpacas, goats, sheep, donkeys, ponies, cows and Stan our Highland Steer.

Upon entry into the farm, the staff will give you a personalised run down of where everything is and what's included.

Services available:

- Free BBQ Facilities, you are welcome to bring your own BBQ or picnic lunch
- Lots of tables & chairs in the shade of the peppermint trees
- Shop (ice-creams, chips, lollies & drinks available to purchase)
- Please note, we do not have a café
- EFTPOS facilities
- Wheelchair accessible
