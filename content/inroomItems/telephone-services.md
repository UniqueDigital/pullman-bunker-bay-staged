---
title: Telephone Services
---

**Internal lines:**

To call a room:

Dial 7 + the room number

The Welcome Desk: dial 2

Luggage / Valet Service: dial 2

Housekeeping / Linen: dial 3

Room Service: dial 4

**Restaurant**

Other Side Of The Moon: dial 6

External Calls:

To call within Australia:

Dial 0 + the number

To make a foreign call:

Dial 0 + 0011 for the foreign service + the country code + the number

For the main list of country codes as well as phone costs, contact the Welcome Desk

Dial 2
