---
title: Gymnasium
---

An exclusive space is available for resort guests 24 hours a day. Located at the pool, the gym is equipped with fitness equipment.

Facilities also include showers.

The area is open to adults and children over the age of 16 accompanied by an adult with parental authority.

Use of the equipment is the sole responsibility of the user.

Towels are available in your villa. The resort shall not be held responsible in any way whatsoever for any loss or damage to guests’ personal effects.

For detailed information, please contact the Welcome Desk.

Dial 2.
