---
title: No Smoking Areas / Smoking Areas
---

Due to licensing laws, our resort is entirely no smoking (including villas, restaurant, banquets and some public spaces located in our licensed premises boundary). Designated smoking areas are set up on the terrace of your room and at the entrance of the resort. Please discard your cigarette butts

Thoughtfully, as the resort has a high fire hazard rating and many small native animals live in the surrounds of the resort. Cigarette butts are toxic and can cause death to our local wildlife. Guests found to be smoking in their villa shall be subject to a penalty fee of $250.
