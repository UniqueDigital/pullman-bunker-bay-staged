---
id: 2
title: Valuables
details:
---

Locker facilities are provided, however we cannot accept responsibility for loss of valuables.
