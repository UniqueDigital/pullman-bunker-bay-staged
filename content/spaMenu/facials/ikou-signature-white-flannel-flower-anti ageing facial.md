---
id: 0
title: Ikou Signature White Flannel Flower Anti Ageing Facial
details: 90mins | $230
---

Showcasing all signature techniques and sensory surprises from the full menu of iKOU Facials, this journey of treatments delivers high- performance, visible Anti-Ageing results. Radiant skin is revealed with an AHA & BHA Australian Desert Lime Face Polish and toning and firming of the skin, hydrating and boosting collagen is achieved using active, organic ingredients, serums and masques extracted from unique, high- performance Australian plant, fruit and flowers infused into the skin and boosted with acupressure facial massage. This beautiful, detailed facial incorporates Aromatherapy scalp, foot, arm and lower leg massage with Australian White Flannel Flower, to inspire a blissful experience.
