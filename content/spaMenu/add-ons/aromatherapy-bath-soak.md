---
id: 0
title: Aromatherapy Bath Soak
details: 30mins | $85
---

Water, the source of life. Utilise the therapeutic benefits of hydrotherapy as a blissful add on before or after your treatment.
