---
title: Dryer
---

To utilise your in-villa dryer, please follow the operating sequence below:

- Load the clothes into the dryer
- Close the door firmly – dryer will not start if door is not closed
- Turning the knob clockwise, select the desired drying cycle/time
- The dryer may stop intermittently to redistribute the clothing and realign the tumbler
- When the dryer has finished it’s cycle, it will commence the automatic airing and cool down cycle
- Once the dryer has completed the cool down cycle, remove items from the dryer.
