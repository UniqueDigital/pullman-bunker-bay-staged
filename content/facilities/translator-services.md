---
title: Translator Services
---

With over 25 nationalities represented at the resort, many foreign languages are spoken by our friendly team.

Alternatively, should you require extra discretion, we can arrange a translator and interpreting service for you at a nominal charge. For assistance contact the Welcome Desk.

Dial 2
