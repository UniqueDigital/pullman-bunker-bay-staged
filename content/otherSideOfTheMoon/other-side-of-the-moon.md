---
title: Other Side of the Moon
image: hero.jpg
alt: hero Image
phone: '61897569100'
website: https://www.countrylifefarm.com.au
email: 'H8775@accor.com'
addressTitle: 'Address:'
addressDesc: Pullman Bunker Bay Resort
addressDescTwo: 42 Bunker Bay Road Naturaliste WA 6281
openingHoursTitle: 'Open 7 Days'
openingHoursDetails: 'Lunch Menu: 12:00pm daily until 5pm'
openingHoursDetailsTwo: 'Dinner Menu: served from 5pm nightly'
offerTitle: ~
offerDescription: ~
deliveryOptionTitle: Bookings
deliveryfee: Due to high demand currently being experienced for dinner, bookings are essential to avoid disappointment.
deliveryDisclaimer: ~
pdfOneTitle: All Day Dining Menu
pdfOne: all-day-menu.pdf
pdfTwoTitle: Beverages Menu
pdfTwo: beverage-list.pdf
facebook: ~
instagram: ~
twitter: ~
bookurl: https://dishcult.com/restaurant/othersideofthemoonaccor7119?sortOrder=0&page=1&bookingDate=2021-12-10&bookingTime=14%3A00&covers=2&promotionId=82280
gallery:
  - hero.jpg
  - image1.jpg
  - image2.jpg
  - image3.jpg
  - image4.jpg
  - image5.jpg
  - image6.jpg
  - image7.jpg
  - image8.jpg
  - image9.jpg
  - image10.jpg
---

Aptly named after nearby Other Side of the Moon surf break - a magical place where you can watch the moon rise and set over the ocean - award-winning Other Side of the Moon Restaurant and Bar specialises in showcasing the Margaret River Region's world renowned local produce.

Executive Chef Kiren Mainwaring has ensured the direction for his range of dishes highlights the amazing produce we have right here in the South West. He has drawn on his many years of experience and designed a menu that allows the produce to speak for itself.

RECENT AWARDS

- WINNER - 2019 Gold Plate Awards - 'Fresh Produce'
- FINALIST - 2019 Gold Plate Awards - 'Formal Contemporary'

#### RESTAURANT

Starring a who's who of the Margaret River's local best, the Other Side of the Moon Restaurant menu pays homage to local and indigenous ingredients that flourish in Bunker Bay during the Wardandi six seasons.

Indulge in succulent, locally bred Linley Valley pork belly and Margaret River Wagyu beef, Denmark mushrooms, foraged herbs, Manjimup truffle oil, Cambray Manchego and apricot quandong chutney - just to name a few! - complemented by decadent desserts from Pullman's Michelin-star experienced French Pastry Chef Romain Lassiaille.

#### LOUNGE BAR

The perfect place to catch up and chill out, Other Side of the Moon Restaurant & Bar offers an exclusive area to sit back and relax and enjoy an array of local and imported wines, beers and cocktails.

Limestone and slate accents in the bar’s design are complemented by the warmth karri and jarrah, with an ambience that truly comes alive after dusk. In winter months the open stone fireplaces are a blissful spot to indulge in the region’s applauded Cabernet Sauvignon wines.

##### THE SOCIAL HOUR

The Social Hour at Pullman is inspired by the cultural and culinary tradition of enjoying the aperitif time. Slow down. Take time to socialise, relax, catch up with friends. Indulge in an array of promotional beverages on offer including hand-crafted cocktails from our talented mixologists for just $15, between 3:00pm to 5:00pm daily with complimentary nibbles for your table.
