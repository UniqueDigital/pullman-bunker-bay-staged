---
id: 4
title: early access to global accor promotions
first: check.svg
second: check.svg
third: check.svg
---

In addition, your Accor Plus membership gives you early access to Accor Global Sales. Not only will you access the Accor Super and Private sales one day in advance, you will also enjoy an additional 10% off the promotion rate.
