---
id: 0
title: HAND PRINT
details: 60mins | $110
---

Our signature manicure. Replenish, repair and soften with exfoliation,
massage, nail care and mask. Includes a colour polish.
